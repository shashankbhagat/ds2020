@extends('layouts.app')


@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <a  class="btn btn-primary float-right" href="{{route('admin-post-form')}}"> Add </a>
    </div>
    <div class="col-md-12">
      <div class="card">
        <div class="card-body">
          <table class="table table-striped">
            <thead>
              <tr>
                <th>#</th>
                <th>Title</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              @for($i = 0; $i < 10; $i++ )
              <tr>
                <td>{{$i+1}}</td>
                <td>Title {{$i+1}}</td>
                <td> <a class="btn btn-xs btn-warning" href="{{route('admin-post-edit')}}"> Edit </a> </td>
              </tr>
              @endfor
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
