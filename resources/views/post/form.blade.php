@extends('layouts.app')


@section('content')

<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-body">
          <form class="" action="{{route('admin-post-save')}}" method="post">
            @csrf
            <div class="form-group">
              <label for="">Tite</label>
              <input type="text" class="form-control" name="post_title" value="">
            </div>
            <div class="form-group">
              <!-- <label for="">Description</label>
              <textarea class="form-control" rows="8" name="post_description" width="100%">
              </textarea> -->
              <div id="example"></div>
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-success"> Save </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
</div>

@endsection


@section('afterScript')

<script>
  var editor = new FroalaEditor('#example');

</script>
@endsection
