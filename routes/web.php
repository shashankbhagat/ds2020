<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/article/all','ArticleController@all_articles')->name('all_articles');
Route::get('/article/read','ArticleController@read_article')->name('read_article');

Route::get('/admin/article/insert','ArticleController@insert_article')->name('insert_article');
Route::post('/admin/article/save','ArticleController@article_save')->name('article_save');


Route::get('/admin/posts','PostController@index')->name('admin-post-index');
Route::get('/admin/post/form','PostController@form')->name('admin-post-form');
Route::post('/admin/post/save','PostController@save')->name('admin-post-save');
Route::get('/admin/post/edit','PostController@edit')->name('admin-post-edit');
Route::get('/admin/post/edit/save','PostController@edit_save')->name('admin-post-edit-save');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
