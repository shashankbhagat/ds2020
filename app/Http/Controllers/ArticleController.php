<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

use App\Article;

class ArticleController extends Controller
{
    //
    public function all_articles()
    {
        $data = [];
        $posts = Article::get();
        // $posts = DB::table('my_articles')->get();
        // dd($posts);
        $data['all_posts'] = $posts;
        $data['students'] = ['shalini','deeksha','ajith','bhavna'];
        // dd($data['students']);
        return view('all_articles',$data);
    }

    public function read_article()
    {
        echo 'This is page where your sibgle article description comes up';
    }

    public function insert_article()
    {
        return view('insert_article');
    }

    public function article_save(Request $r)
    {
        // dd($r->all());

        $title = $r['title'];
        $description = $r['description'];

        $insert_data = [
            'title' => $title,
            'description' => $description,
        ];
        DB::table('articles')->insert($insert_data);

        // $article  = new Article();
        // $article->title = $r['title'];
        // $article->description = $description;
        // $article->save();

        return back();  

    }
}
