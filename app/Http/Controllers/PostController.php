<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use DB;
use Session;


class PostController extends Controller
{
    //
    public function index()
    {
      // \Session::put('name','Ajith');
      // \Session::put('dept','cse');
      // dd(Session::all());
      // dd();
      $this->data = [];
      // $this->data['name'] = 'ajith';
      return view('post.index',$this->data);
    }

    public function form()
    {
      // dd(\Session::all());
      $this->data = [];
      // $this->data['name'] = 'ajith';
      return view('post.form',$this->data);
    }

    public function save(Request $r)
    {
      // dd($r->all());
      // save operation cades;

      // $date = date('Y-m-d H:i:s');

      // dd($date);
      try {

        $insertData = [
          'title' => $r['post_title'],
          'description' => $r['post_description'],
          'created_at' => date('Y-m-d H:i:s')
        ];

        DB::table('posts')->insert($insertData);

        Session::put('msg_exists',true);
        Session::put('msg_class','success');
        Session::put('msg','Saved successfully');

      } catch (\Exception $e) {
        // dd($e->getMessage());
        Session::put('msg_exists',true);
        Session::put('msg_class','danger');
        Session::put('msg','Failed to save !!!'.$e->getMessage());
      }

      return redirect()->route('admin-post-index');
    }

    public function edit()
    {
      $this->data = [];
      // $this->data['name'] = 'ajith';
      return view('post.editform',$this->data);
    }

    public function edit_save(Request $r)
    {
      dd($r->all());
      // edit operation cades;
      return redirect()->route('post-index');
    }


}
